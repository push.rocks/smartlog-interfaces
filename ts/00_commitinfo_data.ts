/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartlog-interfaces',
  version: '3.0.0',
  description: 'interfaces for the smartlog ecosystem'
}

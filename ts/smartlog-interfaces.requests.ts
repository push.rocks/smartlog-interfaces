import { ILogPackageAuthenticated } from './index.js';

export interface IRequest_SmartlogDestinationReceiver_Any_PostLogPackages {
  method: 'postLogPackages';
  request: {
    logPackages: ILogPackageAuthenticated[];
  };
  response: {};
}
